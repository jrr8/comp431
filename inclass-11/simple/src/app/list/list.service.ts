import { Injectable } from '@angular/core';


@Injectable()
export class ListService {

  constructor() {
    this.components = ["donate", "about", "contact", "home", "list"];
  }

  getComponents(): string {
    return this.components;
  }

}
