import { Component, OnInit } from '@angular/core';
import { ListService } from './list.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  private info: String[]

  constructor(private listServ: ListService) { }

  ngOnInit() {
  	this.info = this.listServ.getComponents();
  }

}
