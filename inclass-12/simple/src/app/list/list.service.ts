import { Injectable } from '@angular/core';


@Injectable()
export class ListService {
  private components: String[];

  constructor() {
    this.components = ["donate", "about", "contact", "home", "list"];
  }

  getComponents(): String[] {
    return this.components;
  }

}
