import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-donate',
  templateUrl: './donate.component.html',
  styleUrls: ['./donate.component.css']
})

export class DonateComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

export class Donor implements OnInit {

  constructor(private id: Number, private name: String, private image: String,
              private featured: Boolean, private description: String) { }

  ngOnInit() {
    this.id = this.id;
    this.name = this.name;
    this.image = this.image;
    this.featured = this.featured;
    this.description = this.description;
  }
}

export const DONORS = [new Donor(1, "Dave", "https://upload.wikimedia.org/wikipedia/en/thumb/4/4e/DWLeebron.jpg/220px-DWLeebron.jpg", true, "A cool dude")];
