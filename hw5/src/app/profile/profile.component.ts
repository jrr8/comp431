import { Component, OnInit } from '@angular/core';
import { Profile } from "./profile";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {

  user: Profile;

	constructor( ){}

	ngOnInit(): void {

	  const sampleUser: Profile = {
      firstName: "Riley",
      lastName: "Robertson",
      email: "joe@dirt.com",
      birthday: "01/30/1996",
      username: "RyGuy",
      zip: "12345",
      phone: "(555)555-1234"
    };

    this.user = sampleUser;

  };

	updateUser(first: string, last: string, email: string, username: string, zip: string, phone: string): void {

	  if (/^[A-Z]+[a-zA-Z\-]*$/.test(first)) {
      this.user.firstName = first;
    }
    if (/^[A-Z]+[a-zA-Z\-]*$/.test(last)) {
      this.user.lastName = last;
    }
    if (/^[^\s]+@[^\s]+.[a-z]+$/.test(email)) {
      this.user.email = email;
    }
    if (/^[^\d\s][a-zA-Z0-9]*$/.test(username)) {
      this.user.username = username;
    }
    if (/^[\d]{5}$/.test(zip)) {
      this.user.zip = zip;
    }
	  if (/^\([\d]{3}\)[\d]{3}-[\d]{4}$/.test(phone)) {
      this.user.phone = phone;
    }

  }

  showChange(img: HTMLImageElement, btn: HTMLDivElement) {
	  img.style.filter = "opacity(50%)";
    btn.hidden = false;
  }

  hideChange(img: HTMLImageElement, btn: HTMLDivElement) {
    img.style.filter = "";
    btn.hidden = true;
  }


}
