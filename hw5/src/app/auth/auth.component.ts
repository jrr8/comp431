import { Component, OnInit } from '@angular/core';
import { AuthService } from "./auth.service";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
  providers: [AuthService]
})
export class AuthComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  register(uName: HTMLInputElement, dName: HTMLInputElement, email: HTMLInputElement, phone: HTMLInputElement,
           bDay: HTMLInputElement, zip: HTMLInputElement, pwd: HTMLInputElement, confPwd: HTMLInputElement) {

    const errorModal = document.getElementById('errorModal');
    this.authService.register(uName, dName, email, phone, bDay, zip, pwd, confPwd, errorModal)
  }

}
