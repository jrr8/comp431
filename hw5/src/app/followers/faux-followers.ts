import { Follower } from './follower';

export const FOLLOWERS: Follower[] = [
  { status: "dis my status", name: "Joe", img: "https://vignette.wikia.nocookie.net/ronaldmcdonald/images/2/29/Ronald_McDonald_waving.jpg/revision/latest?cb=20150520002023"},
  { status: "and dis my status", name: "Barry", img: "https://i.ytimg.com/vi/6fPliInHYnE/maxresdefault.jpg"},
  { status: "i like 2 party", name: "Jamm", img: "https://vignette.wikia.nocookie.net/villains/images/2/24/Jamm.jpg/revision/latest?cb=20150404041140" }
];
