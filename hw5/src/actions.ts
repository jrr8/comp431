import fetch from 'mock-fetch';

const url = 'https://webdev-dummy.herokuapp.com'

const resource = (method, endpoint, payload) => {

  const headers = new Headers();
  headers.append('Content-Type', 'application/json');

  const options = {
    method,
    credentials: 'include',
    headers: headers
  };

  if (payload) {
    options['body'] = JSON.stringify(payload);
  }

  return fetch(`${url}/${endpoint}`, options)
    .then(r => {
      if (r.status === 200) {
        if (r.headers.get('Content-Type').indexOf('json') > 0) {
          return r.json()
        } else {
          return r.text()
        }
      } else {
        // useful for debugging, but remove in production
        console.error(`${method} ${endpoint} ${r.statusText}`)
        throw new Error(r.statusText)
      }
    })
}


const updateError = () => {

}
