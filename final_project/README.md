# RiceBook

This submission utilizes Google Oauth for 3rd party authentication. Log in with a google account, or register an account and then log in, or use the test credentials username: "test" password: "pwd"
Cookies are used to establish sessions
To get a user's feed, the backend makes one query to Mongo that returns at most 10 articles from the articles collection, where only articles that the logged in user or one of their followers is they author. Note that the sample data and the test user ("test"/"pwd") has two followers and sees about 6 articles. If they had more followers or their followers posted more articles, you'd get no more than 10 in the feed

