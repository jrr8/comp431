import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { backendURL } from '../app.module';
import { Observable } from "rxjs/Observable";


@Injectable()
export class AuthService {

  constructor(private http: HttpClient) { }

  register(userName: HTMLInputElement, displayName: HTMLInputElement, email: HTMLInputElement,
           phone: HTMLInputElement, birthday: HTMLInputElement, zipcode: HTMLInputElement,
           password: HTMLInputElement): Observable<JSON> {

    return this.http.post(backendURL + "/register", { username: userName.value, displayName: displayName.value, email: email.value,
                                                          phone: phone.value, dob: birthday.value, zip: zipcode.value, password: password.value });


  }t

  login(username: string, password: string): Observable<Object> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.post(backendURL + "/login", { username: username, password: password }, { headers: headers, withCredentials: true })
  }

  logout(): Observable<string> {
    return this.http.put(backendURL + '/logout', { }, { withCredentials: true })
  }

}

