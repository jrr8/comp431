import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from "./auth.service";


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
  providers: [AuthService]
})


export class AuthComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router, private activeRoute: ActivatedRoute) { }

  ngOnInit() {

  }

  register(uName: HTMLInputElement, dName: HTMLInputElement, email: HTMLInputElement, phone: HTMLInputElement,
           bDay: HTMLInputElement, zip: HTMLInputElement, pwd: HTMLInputElement, confPwd: HTMLInputElement, form: HTMLFormElement) {

    if (pwd.value !== confPwd.value || [uName, email, phone, bDay, zip, pwd, confPwd].filter(i => { return i.value === ""}).length > 0) {
      return
    }

    this.authService.register(uName, dName, email, phone, bDay, zip, pwd).subscribe(_ => {
      // currently, upon registration nothing happens, but newly registered user can log in
      form.reset();
      window.scrollTo(0, 0);
    });

  }

}
