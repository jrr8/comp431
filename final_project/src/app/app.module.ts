export const backendURL = 'https://chilling-asylum-94672.herokuapp.com';
//export const backendURL = 'http://127.0.0.1:3000';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from "@angular/common/http";
import { FilePickerModule } from 'angular-file-picker';

import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { HomeComponent } from './home/home.component';
import { NotfoundComponent } from './404/404.component';
import { ProfileComponent } from './profile/profile.component';


export const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'profile', component: ProfileComponent },
  { path: '', component: AuthComponent },
  { path: '**', component: NotfoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    HomeComponent,
    NotfoundComponent,
    AuthComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FilePickerModule,
    HttpClientModule
  ],
  bootstrap: [AppComponent]
})

export class AppModule {

}
