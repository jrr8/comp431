import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";

const backendURL = 'https://chilling-asylum-94672.herokuapp.com';

@Injectable()
export class HomeService {

  constructor(private http: HttpClient) { }

  loadAvatars(users: [string]): Observable<Object> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.get(backendURL + "/avatars/" + users.join(','), { headers: headers, withCredentials: true });
  }

}
