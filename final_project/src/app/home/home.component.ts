import { Component, OnInit } from '@angular/core';
import { Post } from '../post/post';
import { PostService } from '../post/post.service';
import { Follower } from '../followers/follower';
import { FollowersService } from '../followers/followers.service';
import { HomeService } from "./home.service";
import { fetch } from "../app.component";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [PostService, FollowersService, HomeService]
})

export class HomeComponent implements OnInit {

  posts: Post[];
  followers: Follower[];
  username: string;
  status: string;
  isStatusBeingEdited: boolean;
  editOrSave: string;
  photo: string;


  constructor( private postService: PostService, private followerService: FollowersService, private homeService: HomeService ) { }


  ngOnInit() {

    this.postService.getPosts().subscribe(posts => this.posts = posts);
    this.followerService.getFollowers().then(followers => this.followers = followers);
    this.homeService.loadAvatars([fetch('username', "")]).subscribe(r => {
        this.photo = r? r[0].value : 'https://upload.wikimedia.org/wikipedia/en/8/86/Avatar_Aang.png';
    }, err => {
      this.photo = 'https://upload.wikimedia.org/wikipedia/en/8/86/Avatar_Aang.png';
    });

    this.username = fetch("username");

    this.status = 'I like Angular';
    this.isStatusBeingEdited = false;
    this.editOrSave = 'Edit';

  }

  addPost(postArea: HTMLTextAreaElement) {
    this.postService.addPost(postArea, 'Dr. Funk', this.posts).then(posts => {
      this.posts = posts;
    });
  }

  updateStatus(newStatus: string): void {

    if (this.editOrSave === 'Edit') {
      this.isStatusBeingEdited = true;
      this.editOrSave = 'Save';

    } else {
      this.isStatusBeingEdited = false;
      this.editOrSave = 'Edit';
      this.status = newStatus;
    }

  }

  clearPost(postArea: HTMLTextAreaElement): void {
    postArea.value = '';
  }

  addFollower(followerInput: HTMLInputElement) {
    this.followerService.makeFollower(followerInput).then(follower => {
      this.followers.unshift(follower);
    });
  }

  delFollower(oldFollower: Follower) {
    this.followerService.removeFollower(oldFollower, this.followers).then(followers => {
      this.followers = followers
    })

  }

}
