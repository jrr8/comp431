import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { Post } from "./post";

const backendURL = 'https://chilling-asylum-94672.herokuapp.com';


@Injectable()
export class PostService {

  constructor(private http: HttpClient) { }

  getPosts(): Observable<Post[]> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(backendURL + "/articles", { headers: headers, withCredentials: true })
  }

  addPost(postArea: HTMLTextAreaElement, author: string, posts: Post[]): Promise<Post[]> {
    const newPost: Post = {
      text: postArea.value,
      img: null,
      date: function() {
        const date = new Date();
        return date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
      }(),
      comments: [],
      author: author
    };

    postArea.value = "";

    posts.unshift(newPost);

    return Promise.resolve(posts)
  }

}
