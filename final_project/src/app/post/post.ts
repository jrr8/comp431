export class Post {
  text: string;
  img: string;
  date: string;
  comments: object;
  author: string;
}
