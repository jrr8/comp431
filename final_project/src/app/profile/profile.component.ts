import { Component, OnInit } from '@angular/core';
import { Profile } from "./profile";
import { fetch } from "../app.component";
import { HomeService } from "../home/home.service";
import { PickedFile } from "angular-file-picker";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [HomeService]
})

export class ProfileComponent implements OnInit {

  user: Profile;
  photo: string;
  picked: PickedFile;

	constructor(private homeService: HomeService) { }

	ngOnInit(): void {

	  const sampleUser: Profile = {
      firstName: "Riley",
      lastName: "Robertson",
      email: "joe@dirt.com",
      birthday: "01/30/1996",
      username: "RyGuy",
      zip: "12345",
      phone: "(555)555-1234"
    };

    this.user = sampleUser;

    this.homeService.loadAvatars([fetch('username', "")]).subscribe(r => {
      this.photo = r? r[0].value : 'https://upload.wikimedia.org/wikipedia/en/8/86/Avatar_Aang.png';
    }, err => {
      this.photo = 'https://upload.wikimedia.org/wikipedia/en/8/86/Avatar_Aang.png';
    });

  };

	updateUser(first: string, last: string, email: string, username: string, zip: string, phone: string): void {

	  if (/^[A-Z]+[a-zA-Z\-]*$/.test(first)) {
      this.user.firstName = first;
    }
    if (/^[A-Z]+[a-zA-Z\-]*$/.test(last)) {
      this.user.lastName = last;
    }
    if (/^[^\s]+@[^\s]+.[a-z]+$/.test(email)) {
      this.user.email = email;
    }
    if (/^[^\d\s][a-zA-Z0-9]*$/.test(username)) {
      this.user.username = username;
    }
    if (/^[\d]{5}$/.test(zip)) {
      this.user.zip = zip;
    }
	  if (/^\([\d]{3}\)[\d]{3}-[\d]{4}$/.test(phone)) {
      this.user.phone = phone;
    }

  }

  imgPicked(file: PickedFile) {
	  this.picked = file;
    console.log(this.picked);
  }

  showChange(img: HTMLImageElement, btn: HTMLDivElement) {
	  img.style.filter = "opacity(50%)";
    btn.hidden = false;
  }

  hideChange(img: HTMLImageElement, btn: HTMLDivElement) {
    img.style.filter = "";
    btn.hidden = true;
  }


}
