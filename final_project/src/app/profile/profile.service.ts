import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { backendURL } from '../app.module';
import { Observable } from "rxjs/Observable";
import { PickedFile } from "angular-file-picker";


@Injectable()
export class ProfileService {

  constructor(private http: HttpClient) { }

  sendImg(file: PickedFile): Observable<JSON> {
    const formData = new FormData();
    formData.append('image', file.dataURL)
    return this.http.put(backendURL + "/avatar", {})
  }

}
