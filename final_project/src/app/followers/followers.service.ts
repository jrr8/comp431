import { Injectable } from '@angular/core';
import { Follower } from './follower';
import { FOLLOWERS } from './faux-followers';



@Injectable()
export class FollowersService {

  getFollowers(): Promise<Follower[]> {
    return Promise.resolve(FOLLOWERS);
  }

  makeFollower(newFollower: HTMLInputElement): Promise<Follower> {
    const follower: Follower = {
      name: newFollower.value,
      img: 'http://lorempixel.com/384/202/',
      status: 'Look who decided to show up'
    };

    newFollower.value = '';
    return Promise.resolve(follower);
  }

  removeFollower(oldFollower: Follower, currentFollowers: Follower[]): Promise<Follower[]> {
    const idx = currentFollowers.indexOf(oldFollower);

    if (idx > -1) {
      currentFollowers.splice(idx, 1);
    }

    return Promise.resolve(currentFollowers)
  }

}
