import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from "./auth/auth.service";
import { backendURL } from "./app.module";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AuthService]
})

class AppComponent implements OnInit {
  title: string = "RiceBook";
  loggedIn: boolean;
  userName: string;
  password: string;
  url: string;

  constructor(private router: Router, private activeRoute: ActivatedRoute, private auth: AuthService) { };


  ngOnInit(): void {
    this.userName = fetch("username");
    this.password = fetch("password");
    this.loggedIn = Boolean(this.userName && this.password);
    this.url = backendURL;
    if (this.loggedIn) {
      this.router.navigate(['home'], { relativeTo: this.activeRoute.root });
    }
  }

  logIn(): void {
    this.auth.login(this.userName, this.password).subscribe( r => {
      this.loggedIn = true;
      window.localStorage.setItem("username", this.userName);
      window.localStorage.setItem("password", this.password);
      console.log(this.activeRoute.root)
      this.router.navigate(['home'], { relativeTo: this.activeRoute.root });
    }, e => {
      // show user login error here
    });
  };

  logOut(): void {
    this.auth.logout().subscribe(r => {
      this.loggedIn = false;
      window.localStorage.removeItem("username");
      this.userName = "";
      window.localStorage.removeItem("password");
      this.password = "";
      this.router.navigate(['/'], { relativeTo: this.activeRoute.root })
    }, err => {
      this.loggedIn = false;
      window.localStorage.removeItem("username");
      this.userName = "";
      window.localStorage.removeItem("password");
      this.password = "";
      this.router.navigate(['/'], { relativeTo: this.activeRoute.root })
    })
  }

  gLogin(): void {
    window.localStorage.setItem("username", this.userName + "Google User");
    window.localStorage.setItem("password", this.password + "Gpwd");
  }



}


function fetch(key, def = "") {
  if (window.localStorage.getItem((key)) !== null) {
    return window.localStorage.getItem((key))
  } else {
    return def
  }
}

export { AppComponent, fetch }
