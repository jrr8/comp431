webpackJsonp(["main"],{

/***/ "../../../../../src/$$_gendir lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_gendir lazy recursive";

/***/ }),

/***/ "../../../../../src/app/404/404.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".round {\n  border-radius: 20px;\n  max-width: 75%;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/404/404.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"jumbotron mt-5 mx-auto round\">\n    <h1 class=\"display-3\">Oops...</h1>\n    <hr class=\"my-4\">\n    <p class=\"text-center\">There's nothing here ;(</p>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/404/404.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotfoundComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NotfoundComponent = (function () {
    function NotfoundComponent() {
    }
    NotfoundComponent.prototype.ngOnInit = function () {
    };
    return NotfoundComponent;
}());
NotfoundComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-notfound',
        template: __webpack_require__("../../../../../src/app/404/404.component.html"),
        styles: [__webpack_require__("../../../../../src/app/404/404.component.css")]
    }),
    __metadata("design:paramtypes", [])
], NotfoundComponent);

//# sourceMappingURL=404.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input {\n  max-width: 10em;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-sm navbar-dark bg-dark d-flex justify-content-start\">\n\n  <a class=\"navbar-brand mr-4 pr-0\" routerLink=\"/\" routerLinkActive=\"active\" [hidden]=\"loggedIn\"><h2>{{title}}</h2></a>\n  <a class=\"navbar-brand mr-4 pr-0\" routerLink=\"/home\" routerLinkActive=\"active\" [hidden]=\"!loggedIn\"><h2>{{title}}</h2></a>\n\n  <ul class=\"navbar-nav p-0\">\n    <li class=\"nav-item\">\n      <a class=\"nav-link\" routerLink=\"/home\" routerLinkActive=\"active\" [hidden]=\"!loggedIn\">Home</a>\n    </li>\n    <li class=\"nav-item\">\n      <a class=\"nav-link\" routerLink=\"/profile\" routerLinkActive=\"active\" [hidden]=\"!loggedIn\">Profile</a>\n    </li>\n  </ul>\n\n\n  <input #nameInput class=\"form-control ml-auto mr-sm-2\" type=\"text\" placeholder=\"Username\" [hidden]=\"loggedIn\"  [value]=\"userName\" (change)=\"userName=nameInput.value\" required>\n  <input #pwdInput class=\"form-control mr-sm-2\" type=\"password\" placeholder=\"Passphrase\" [hidden]=\"loggedIn\" [value]=\"password\" (change)=\"password=pwdInput.value\" required>\n  <input #searchInput class=\"form-control ml-auto mr-sm-2\" type=\"text\" placeholder=\"Search Posts\" [hidden]=\"!loggedIn\" required>\n  <button class=\"btn btn-outline-success my-2 my-sm-0\" (click)=\"logIn()\" [hidden]=\"loggedIn\">Log In</button>\n  <a href=\"{{url}}/auth/google\" class=\"btn btn-outline-primary ml-2\" [hidden]=\"loggedIn\" (click)=\"gLogin()\">Google Sign In</a>\n  <button class=\"btn btn-outline-success my-2 my-sm-0 mr-2\" [hidden]=\"!loggedIn\">Search</button>\n  <button class=\"btn btn-outline-success my-2 my-sm-0\" (click)=\"logOut()\" [hidden]=\"!loggedIn\">Log Out</button>\n\n</nav>\n\n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fetch; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_auth_service__ = __webpack_require__("../../../../../src/app/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppComponent = (function () {
    function AppComponent(router, activeRoute, auth) {
        this.router = router;
        this.activeRoute = activeRoute;
        this.auth = auth;
        this.title = "RiceBook";
    }
    ;
    AppComponent.prototype.ngOnInit = function () {
        this.userName = fetch("username");
        this.password = fetch("password");
        this.loggedIn = Boolean(this.userName && this.password);
        this.url = __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* backendURL */];
        if (this.loggedIn) {
            this.router.navigate(['home'], { relativeTo: this.activeRoute.root });
        }
    };
    AppComponent.prototype.logIn = function () {
        var _this = this;
        this.auth.login(this.userName, this.password).subscribe(function (r) {
            _this.loggedIn = true;
            window.localStorage.setItem("username", _this.userName);
            window.localStorage.setItem("password", _this.password);
            console.log(_this.activeRoute.root);
            _this.router.navigate(['home'], { relativeTo: _this.activeRoute.root });
        }, function (e) {
            // show user login error here
        });
    };
    ;
    AppComponent.prototype.logOut = function () {
        var _this = this;
        this.auth.logout().subscribe(function (r) {
            _this.loggedIn = false;
            window.localStorage.removeItem("username");
            _this.userName = "";
            window.localStorage.removeItem("password");
            _this.password = "";
            _this.router.navigate(['/'], { relativeTo: _this.activeRoute.root });
        }, function (err) {
            _this.loggedIn = false;
            window.localStorage.removeItem("username");
            _this.userName = "";
            window.localStorage.removeItem("password");
            _this.password = "";
            _this.router.navigate(['/'], { relativeTo: _this.activeRoute.root });
        });
    };
    AppComponent.prototype.gLogin = function () {
        window.localStorage.setItem("username", this.userName + "Google User");
        window.localStorage.setItem("password", this.password + "Gpwd");
    };
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.css")],
        providers: [__WEBPACK_IMPORTED_MODULE_2__auth_auth_service__["a" /* AuthService */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__auth_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__auth_auth_service__["a" /* AuthService */]) === "function" && _c || Object])
], AppComponent);
function fetch(key, def) {
    if (def === void 0) { def = ""; }
    if (window.localStorage.getItem((key)) !== null) {
        return window.localStorage.getItem((key));
    }
    else {
        return def;
    }
}

var _a, _b, _c;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return backendURL; });
/* unused harmony export routes */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular_file_picker__ = __webpack_require__("../../../../angular-file-picker/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__auth_auth_component__ = __webpack_require__("../../../../../src/app/auth/auth.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__home_home_component__ = __webpack_require__("../../../../../src/app/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__404_404_component__ = __webpack_require__("../../../../../src/app/404/404.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__profile_profile_component__ = __webpack_require__("../../../../../src/app/profile/profile.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var backendURL = 'https://chilling-asylum-94672.herokuapp.com';
//export const backendURL = 'http://127.0.0.1:3000';










var routes = [
    { path: 'home', component: __WEBPACK_IMPORTED_MODULE_7__home_home_component__["a" /* HomeComponent */] },
    { path: 'profile', component: __WEBPACK_IMPORTED_MODULE_9__profile_profile_component__["a" /* ProfileComponent */] },
    { path: '', component: __WEBPACK_IMPORTED_MODULE_6__auth_auth_component__["a" /* AuthComponent */] },
    { path: '**', component: __WEBPACK_IMPORTED_MODULE_8__404_404_component__["a" /* NotfoundComponent */] }
];
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["M" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_9__profile_profile_component__["a" /* ProfileComponent */],
            __WEBPACK_IMPORTED_MODULE_7__home_home_component__["a" /* HomeComponent */],
            __WEBPACK_IMPORTED_MODULE_8__404_404_component__["a" /* NotfoundComponent */],
            __WEBPACK_IMPORTED_MODULE_6__auth_auth_component__["a" /* AuthComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* RouterModule */].forRoot(routes),
            __WEBPACK_IMPORTED_MODULE_4_angular_file_picker__["a" /* FilePickerModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClientModule */]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/auth/auth.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".round {\n  border-radius: 15px;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/auth/auth.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n\n  <div class=\"jumbotron mt-5 round\">\n    <h1 class=\"display-3\">Sign up for RiceBook</h1>\n    <hr class=\"my-4\">\n    <p>Sign up and start sharing with your friends today.</p>\n  </div>\n\n  <h3 class=\"ml-5 mb-3\">Tell us about yourself</h3>\n  <hr class=\"my-4\">\n  <h6 class=\"form-text text-muted ml-5 mb-5\">Fields marked with an asterisk (*) are mandatory</h6>\n\n  <div class=\"row mb-5 d-flex\">\n    <div class=\"col-6 mx-auto w-100\">\n      <form #registerForm>\n        <div class=\"form-group\">\n          <div class=\"d-flex justify-content-between\">\n            <label for=\"inputUsername\">Username</label>\n            <small class=\"text-danger mr-1\">*</small>\n          </div>\n          <input #uName type=\"text\" class=\"form-control\" id=\"inputUsername\" placeholder=\"John513\" pattern=\"^[^\\d\\s][a-zA-Z0-9]*$\"\n                 title=\"Only upper and lowercase letters plus numbers. Don't start with a number\" required>\n          <small class=\"form-text text-muted\">This is the name you will log in with</small>\n        </div>\n        <div class=\"form-group\">\n          <label for=\"inputDisplayName\">Display Name</label>\n          <input #dName type=\"text\" class=\"form-control\" id=\"inputDisplayName\" placeholder=\"Johnny Appleseed\"\n                 title=\"Only upper and lowercase letters plus numbers. Don't start with a number\" pattern=\"^[^\\d\\s][a-zA-Z0-9]*$\">\n          <small class=\"form-text text-muted\">This is the name other users will see for you</small>\n        </div>\n        <div class=\"form-group\">\n          <div class=\"d-flex justify-content-between\">\n            <label for=\"inputEmail1\">Email address</label>\n            <small class=\"text-danger mr-1\">*</small>\n          </div>\n          <input #email type=\"email\" class=\"form-control\" id=\"inputEmail1\" aria-describedby=\"emailHelp\" placeholder=\"Enter email\"\n                 pattern=\"[^\\s]+@[^\\s]+.[a-z]+\" required>\n          <small class=\"form-text text-muted\">We'll never share your email with anyone else.</small>\n        </div>\n        <div class=\"form-group\">\n          <div class=\"d-flex justify-content-between\">\n            <label for=\"inputPhone\">Phone</label>\n            <small class=\"text-danger mr-1\">*</small>\n          </div>\n          <input #phone type=\"text\" class=\"form-control\" id=\"inputPhone\" placeholder=\"(555)555-1234\"\n                 title=\"Format: (555)555-1234\" pattern=\"\\(\\d\\d\\d\\)\\d\\d\\d-\\d\\d\\d\\d\" required>\n        </div>\n        <div class=\"form-group\">\n          <div class=\"d-flex justify-content-between\">\n            <label for=\"inputBirthday\">Birthday</label>\n            <small class=\"text-danger mr-1\">*</small>\n          </div>\n          <input #bDay type=\"date\" class=\"form-control\" id=\"inputBirthday\" pattern=\"\\d\\d/\\d\\d/\\d\\d\\d\\d\"\n                 title=\"You must be at least 18 years old to use RiceBook\" required>\n          <small class=\"form-text text-muted\">You must be at least 18 years old to use RiceBook.</small>\n        </div>\n        <div class=\"form-group\">\n          <div class=\"d-flex justify-content-between\">\n            <label for=\"inputZip\">Zip Code</label>\n            <small class=\"text-danger mr-1\">*</small>\n          </div>\n          <input #zip type=\"number\" class=\"form-control\" id=\"inputZip\" placeholder=\"12345\"\n                 pattern=\"\\d\\d\\d\\d\\d\" title=\"e.g. 00123\" required>\n        </div>\n        <div class=\"form-group\">\n          <div class=\"d-flex justify-content-between\">\n            <label for=\"inputPassword\">Password</label>\n            <small class=\"text-danger mr-1\">*</small>\n          </div>\n          <input #pwd type=\"password\" class=\"form-control\" id=\"inputPassword\" placeholder=\"Password\"\n                 pattern=\"^\\S+$\" title=\"Passwords cannot contain spaces\" required>\n          <small class=\"form-text text-muted\">Passwords cannot contain spaces</small>\n        </div>\n        <div class=\"form-group\">\n          <div class=\"d-flex justify-content-between\">\n            <label for=\"confirmPassword\">Confirm Password</label>\n            <small class=\"text-danger mr-1\">*</small>\n          </div>\n          <input #confPwd type=\"password\" class=\"form-control\" id=\"confirmPassword\" pattern=\"^\\S+$\" required>\n        </div>\n        <div class=\"d-flex justify-content-between\">\n          <button type=\"reset\" class=\"btn btn-danger\">Clear</button>\n          <button type=\"button\" class=\"btn btn-primary\" (click)=\"register(uName, dName, email, phone, bDay, zip, pwd, confPwd, registerForm)\">Register</button>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/auth/auth.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_service__ = __webpack_require__("../../../../../src/app/auth/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthComponent = (function () {
    function AuthComponent(authService, router, activeRoute) {
        this.authService = authService;
        this.router = router;
        this.activeRoute = activeRoute;
    }
    AuthComponent.prototype.ngOnInit = function () {
    };
    AuthComponent.prototype.register = function (uName, dName, email, phone, bDay, zip, pwd, confPwd, form) {
        if (pwd.value !== confPwd.value || [uName, email, phone, bDay, zip, pwd, confPwd].filter(function (i) { return i.value === ""; }).length > 0) {
            return;
        }
        this.authService.register(uName, dName, email, phone, bDay, zip, pwd).subscribe(function (_) {
            // currently, upon registration nothing happens, but newly registered user can log in
            form.reset();
            window.scrollTo(0, 0);
        });
    };
    return AuthComponent;
}());
AuthComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-auth',
        template: __webpack_require__("../../../../../src/app/auth/auth.component.html"),
        styles: [__webpack_require__("../../../../../src/app/auth/auth.component.css")],
        providers: [__WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _c || Object])
], AuthComponent);

var _a, _b, _c;
//# sourceMappingURL=auth.component.js.map

/***/ }),

/***/ "../../../../../src/app/auth/auth.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthService = (function () {
    function AuthService(http) {
        this.http = http;
    }
    AuthService.prototype.register = function (userName, displayName, email, phone, birthday, zipcode, password) {
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_module__["b" /* backendURL */] + "/register", { username: userName.value, displayName: displayName.value, email: email.value,
            phone: phone.value, dob: birthday.value, zip: zipcode.value, password: password.value });
    };
    AuthService.prototype.login = function (username, password) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]();
        headers = headers.set('Content-Type', 'application/json');
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__app_module__["b" /* backendURL */] + "/login", { username: username, password: password }, { headers: headers, withCredentials: true });
    };
    AuthService.prototype.logout = function () {
        return this.http.put(__WEBPACK_IMPORTED_MODULE_2__app_module__["b" /* backendURL */] + '/logout', {}, { withCredentials: true });
    };
    return AuthService;
}());
AuthService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object])
], AuthService);

var _a;
//# sourceMappingURL=auth.service.js.map

/***/ }),

/***/ "../../../../../src/app/followers/faux-followers.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FOLLOWERS; });
var FOLLOWERS = [
    { status: "dis my status", name: "Joe", img: "https://vignette.wikia.nocookie.net/ronaldmcdonald/images/2/29/Ronald_McDonald_waving.jpg/revision/latest?cb=20150520002023" },
    { status: "and dis my status", name: "Barry", img: "https://i.ytimg.com/vi/6fPliInHYnE/maxresdefault.jpg" },
    { status: "i like 2 party", name: "Jamm", img: "https://vignette.wikia.nocookie.net/villains/images/2/24/Jamm.jpg/revision/latest?cb=20150404041140" }
];
//# sourceMappingURL=faux-followers.js.map

/***/ }),

/***/ "../../../../../src/app/followers/followers.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FollowersService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__faux_followers__ = __webpack_require__("../../../../../src/app/followers/faux-followers.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var FollowersService = (function () {
    function FollowersService() {
    }
    FollowersService.prototype.getFollowers = function () {
        return Promise.resolve(__WEBPACK_IMPORTED_MODULE_1__faux_followers__["a" /* FOLLOWERS */]);
    };
    FollowersService.prototype.makeFollower = function (newFollower) {
        var follower = {
            name: newFollower.value,
            img: 'http://lorempixel.com/384/202/',
            status: 'Look who decided to show up'
        };
        newFollower.value = '';
        return Promise.resolve(follower);
    };
    FollowersService.prototype.removeFollower = function (oldFollower, currentFollowers) {
        var idx = currentFollowers.indexOf(oldFollower);
        if (idx > -1) {
            currentFollowers.splice(idx, 1);
        }
        return Promise.resolve(currentFollowers);
    };
    return FollowersService;
}());
FollowersService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])()
], FollowersService);

//# sourceMappingURL=followers.service.js.map

/***/ }),

/***/ "../../../../../src/app/home/home.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".smallRound {\n  border-radius: 5px;\n}\n\n.displayCard {\n  background: rgb(220, 215, 225);\n  border-radius: 20px;\n}\n\n.postLabel {\n  max-height: 25%;\n  color: rgb(80, 80, 80);\n}\n\n.postText {\n  width: 95%;\n  height: 45%;\n  max-height: 45%;\n}\n\n.editPostBtn {\n  border: none;\n}\n\n.postBtn {\n  border: none;\n  margin-left: 0.5em;\n  margin-right: 0.5em;\n}\n\n.followerImg {\n  max-width: 100%;\n}\n\n.postPic {\n  border-radius: 5px;\n  max-height: 50%;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row justify-content-around mt-5\">\n\n    <div class=\"col col-md-4 col-12 mb-4 displayCard\">\n      <div class=\"text-center mt-4\">\n        <img [src]=\"photo\" width=\"45%\" class=\"mb-4 smallRound\">\n        <h3>{{username}}</h3>\n        <p [(innerHTML)]=\"status\"></p>\n        <input #updateStatusInput class=\"form-control mb-3\" value={{status}} [hidden]=\"!isStatusBeingEdited\">\n        <button class=\"btn btn-outline-primary editPostBtn mb-3\" (click)=\"updateStatus(updateStatusInput.value)\">\n          {{editOrSave}}\n        </button>\n      </div>\n    </div>\n\n\n    <div class=\"col col-md-7 col-12 mb-4 displayCard\">\n      <h2 class=\"postLabel mt-4 mb-3 ml-3\">New Post</h2>\n      <hr>\n      <textarea #newPost class=\"form-control postText mx-auto mb-3\"></textarea>\n\n      <div class=\"d-flex justify-content-center\">\n        <button class=\"btn btn-outline-success postBtn\" (click)=\"addPost(newPost)\">Post</button>\n        <button class=\"btn btn-outline-primary postBtn\" ngFilePicker accept=\"image/*\" (filePick)=\"picked = $event\">\n          Upload\n        </button>\n        <button class=\"btn btn-outline-danger postBtn\" (click)=\"clearPost(newPost)\">Clear</button>\n      </div>\n    </div>\n  </div>\n\n</div>\n\n\n<div class=\"row d-flex justify-content-around ml-3 mt-3\">\n\n  <div class=\"col col-md-7 col-12 displayCard\">\n    <div *ngFor=\"let post of posts\">\n      <div class=\"row\">\n        <div class=\"col-12 col-md-12\">\n          <img src={{post.img}} height=\"200\" class=\"mb-3 mt-4 postPic\"/>\n          <h2 class=\"mx-2\">{{post.author}}</h2>\n          <p class=\"mx-2\">{{post.text}}</p>\n          <p class=\"mx-2\">{{post.date}}</p>\n          <div class=\"d-flex justify-content-center\">\n            <button class=\"btn btn-success mr-2\">Comment</button>\n            <button class=\"btn btn-primary ml-2\">Edit</button>\n          </div>\n        </div>\n      </div>\n      <hr>\n    </div>\n  </div>\n\n\n  <div class=\"col col-md-4 col-12 displayCard align-self-start\">\n    <h4 class=\"postLabel my-4 ml-4\">Your Followed Users</h4>\n    <hr>\n\n    <div *ngFor=\"let follower of followers\">\n      <div class=\"d-flex justify-content-between mb-3\">\n        <div class=\"col-6 text-center\">\n          <h5 class=\"postLabel\">{{follower.name}}</h5>\n          <p>{{follower.status}}</p>\n          <button class=\"btn btn-outline-danger postBtn\" (click)=\"delFollower(follower)\">Unfollow</button>\n        </div>\n        <div class=\"col-6\">\n          <img src=\"{{follower.img}}\" class=\"followerImg smallRound\">\n        </div>\n      </div>\n      <hr>\n    </div>\n\n    <div class=\"d-flex\">\n      <input #newFollower class=\"form-control my-3\" placeholder=\"Username\">\n      <button class=\"btn btn-outline-primary ml-2 my-3\" (click)=\"addFollower(newFollower)\">Add</button>\n    </div>\n\n  </div>\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__post_post_service__ = __webpack_require__("../../../../../src/app/post/post.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__followers_followers_service__ = __webpack_require__("../../../../../src/app/followers/followers.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_service__ = __webpack_require__("../../../../../src/app/home/home.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomeComponent = (function () {
    function HomeComponent(postService, followerService, homeService) {
        this.postService = postService;
        this.followerService = followerService;
        this.homeService = homeService;
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.postService.getPosts().subscribe(function (posts) { return _this.posts = posts; });
        this.followerService.getFollowers().then(function (followers) { return _this.followers = followers; });
        this.homeService.loadAvatars([Object(__WEBPACK_IMPORTED_MODULE_4__app_component__["b" /* fetch */])('username', "")]).subscribe(function (r) {
            _this.photo = r ? r[0].value : 'https://upload.wikimedia.org/wikipedia/en/8/86/Avatar_Aang.png';
        }, function (err) {
            _this.photo = 'https://upload.wikimedia.org/wikipedia/en/8/86/Avatar_Aang.png';
        });
        this.username = Object(__WEBPACK_IMPORTED_MODULE_4__app_component__["b" /* fetch */])("username");
        this.status = 'I like Angular';
        this.isStatusBeingEdited = false;
        this.editOrSave = 'Edit';
    };
    HomeComponent.prototype.addPost = function (postArea) {
        var _this = this;
        this.postService.addPost(postArea, 'Dr. Funk', this.posts).then(function (posts) {
            _this.posts = posts;
        });
    };
    HomeComponent.prototype.updateStatus = function (newStatus) {
        if (this.editOrSave === 'Edit') {
            this.isStatusBeingEdited = true;
            this.editOrSave = 'Save';
        }
        else {
            this.isStatusBeingEdited = false;
            this.editOrSave = 'Edit';
            this.status = newStatus;
        }
    };
    HomeComponent.prototype.clearPost = function (postArea) {
        postArea.value = '';
    };
    HomeComponent.prototype.addFollower = function (followerInput) {
        var _this = this;
        this.followerService.makeFollower(followerInput).then(function (follower) {
            _this.followers.unshift(follower);
        });
    };
    HomeComponent.prototype.delFollower = function (oldFollower) {
        var _this = this;
        this.followerService.removeFollower(oldFollower, this.followers).then(function (followers) {
            _this.followers = followers;
        });
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-home',
        template: __webpack_require__("../../../../../src/app/home/home.component.html"),
        styles: [__webpack_require__("../../../../../src/app/home/home.component.css")],
        providers: [__WEBPACK_IMPORTED_MODULE_1__post_post_service__["a" /* PostService */], __WEBPACK_IMPORTED_MODULE_2__followers_followers_service__["a" /* FollowersService */], __WEBPACK_IMPORTED_MODULE_3__home_service__["a" /* HomeService */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__post_post_service__["a" /* PostService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__post_post_service__["a" /* PostService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__followers_followers_service__["a" /* FollowersService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__followers_followers_service__["a" /* FollowersService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__home_service__["a" /* HomeService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__home_service__["a" /* HomeService */]) === "function" && _c || Object])
], HomeComponent);

var _a, _b, _c;
//# sourceMappingURL=home.component.js.map

/***/ }),

/***/ "../../../../../src/app/home/home.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var backendURL = 'https://chilling-asylum-94672.herokuapp.com';
var HomeService = (function () {
    function HomeService(http) {
        this.http = http;
    }
    HomeService.prototype.loadAvatars = function (users) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]();
        headers = headers.set('Content-Type', 'application/json');
        return this.http.get(backendURL + "/avatars/" + users.join(','), { headers: headers, withCredentials: true });
    };
    return HomeService;
}());
HomeService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object])
], HomeService);

var _a;
//# sourceMappingURL=home.service.js.map

/***/ }),

/***/ "../../../../../src/app/post/post.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PostService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var backendURL = 'https://chilling-asylum-94672.herokuapp.com';
var PostService = (function () {
    function PostService(http) {
        this.http = http;
    }
    PostService.prototype.getPosts = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]().set('Content-Type', 'application/json');
        return this.http.get(backendURL + "/articles", { headers: headers, withCredentials: true });
    };
    PostService.prototype.addPost = function (postArea, author, posts) {
        var newPost = {
            text: postArea.value,
            img: null,
            date: function () {
                var date = new Date();
                return date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
            }(),
            comments: [],
            author: author
        };
        postArea.value = "";
        posts.unshift(newPost);
        return Promise.resolve(posts);
    };
    return PostService;
}());
PostService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object])
], PostService);

var _a;
//# sourceMappingURL=post.service.js.map

/***/ }),

/***/ "../../../../../src/app/profile/profile.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "img {\n  border-radius: 20px;\n}\n\n.imgWrapper {\n  position: relative;\n}\n\n.imgOverlay {\n  position: absolute;\n  top: 0;\n  bottom: 50%;\n  left: 0;\n  right: 0;\n  text-align: center;\n}\n\n.imgOverlay:before {\n  content: ' ';\n  display: block;\n  height: 100%;\n}\n\n.profInfoLeft {\n  margin-left: 50%;\n}\n\n.profInfoRight {\n  margin-right: 50%;\n}\n\n.editInfo {\n  background: rgb(220, 215, 225);\n  border-radius: 20px;\n  width: 90%;\n}\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/profile/profile.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container mt-5\">\n\n  <h1 class=\"text-center\">Your Profile</h1>\n  <div class=\"row d-flex justify-content-center imgWrapper mt-3\">\n    <div (mouseover)=\"showChange(userImg, btnOverlay)\" (mouseout)=\"hideChange(userImg, btnOverlay)\"\n         class=\"text-center w-50\">\n      <img #userImg [src]=\"photo\" class=\"mt-3 w-100\" style=\"max-width: 350px!important\">\n      <div #btnOverlay class=\"imgOverlay\" hidden>\n        <button #changeImgBtn class=\"btn btn-lg btn-primary\" ngFilePicker accept=\"image/*\" (filePick)=\"imgPicked($event)\">\n          Change\n        </button>\n      </div>\n    </div>\n  </div>\n\n  <br>\n  <div class=\"row d-flex justify-content-center w-50 mx-auto mt-3 mb-5\">\n    <div class=\"col-6\">\n      <div><strong>Name: </strong>{{user.firstName}} {{user.lastName}}</div>\n      <div><strong>Display Name: </strong>{{user.username}}</div>\n      <div><strong>Email: </strong>{{user.email}}</div>\n    </div>\n    <div class=\"col-6\">\n      <div><strong>Birthday: </strong>{{user.birthday}}</div>\n      <div><strong>Phone: </strong>{{user.phone}}</div>\n      <div><strong>Zip Code: </strong>{{user.zip}}</div>\n    </div>\n  </div>\n\n  <div class=\"row justify-content-around my-5 mx-auto editInfo\">\n    <div class=\"col col-12 ml-3\">\n      <h2 class=\"mt-5\">Edit Your Info</h2>\n      <hr>\n    </div>\n\n    <div class=\"col-6 mt-3 pl-4\">\n      <div class=\"form-group\">\n        <label for=\"firstName\">First Name</label>\n        <input #firstName type=\"text\" id=\"firstName\" class=\"form-control\" value=\"{{user.firstName}}\">\n      </div>\n      <div class=\"form-group\">\n        <label for=\"lastName\">Last Name</label>\n        <input #lastName type=\"text\" id=\"lastName\" class=\"form-control\" value=\"{{user.lastName}}\">\n      </div>\n      <div class=\"form-group\">\n        <label for=\"username\">Display Name:</label>\n        <input #username type=\"text\" id=\"username\" class=\"form-control\" value=\"{{user.username}}\">\n      </div>\n    </div>\n\n    <div class=\"col-6 mt-3 pr-4\">\n      <div class=\"form-group\">\n        <label for=\"email\">Email:</label>\n        <input #email id=\"email\" type=\"email\" class=\"form-control\" value=\"{{user.email}}\">\n      </div>\n      <div class=\"form-group\">\n        <label for=\"phone\">Phone Number:</label>\n        <input #phone id=\"phone\" type=\"text\" class=\"form-control\" value=\"{{user.phone}}\">\n      </div>\n      <div class=\"form-group\">\n        <label for=\"zipcode\">Zipcode:</label>\n        <input #zip type=\"number\" id=\"zipcode\" class=\"form-control\" value=\"{{user.zip}}\">\n      </div>\n      <br>\n    </div>\n    <button type=\"submit\" class=\"btn btn-primary mb-4\"\n            (click)=\"updateUser(firstName.value, lastName.value, email.value, username.value, zip.value, phone.value)\">\n      Update\n    </button>\n\n  </div>\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/profile/profile.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home_service__ = __webpack_require__("../../../../../src/app/home/home.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProfileComponent = (function () {
    function ProfileComponent(homeService) {
        this.homeService = homeService;
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        var sampleUser = {
            firstName: "Riley",
            lastName: "Robertson",
            email: "joe@dirt.com",
            birthday: "01/30/1996",
            username: "RyGuy",
            zip: "12345",
            phone: "(555)555-1234"
        };
        this.user = sampleUser;
        this.homeService.loadAvatars([Object(__WEBPACK_IMPORTED_MODULE_1__app_component__["b" /* fetch */])('username', "")]).subscribe(function (r) {
            _this.photo = r ? r[0].value : 'https://upload.wikimedia.org/wikipedia/en/8/86/Avatar_Aang.png';
        }, function (err) {
            _this.photo = 'https://upload.wikimedia.org/wikipedia/en/8/86/Avatar_Aang.png';
        });
    };
    ;
    ProfileComponent.prototype.updateUser = function (first, last, email, username, zip, phone) {
        if (/^[A-Z]+[a-zA-Z\-]*$/.test(first)) {
            this.user.firstName = first;
        }
        if (/^[A-Z]+[a-zA-Z\-]*$/.test(last)) {
            this.user.lastName = last;
        }
        if (/^[^\s]+@[^\s]+.[a-z]+$/.test(email)) {
            this.user.email = email;
        }
        if (/^[^\d\s][a-zA-Z0-9]*$/.test(username)) {
            this.user.username = username;
        }
        if (/^[\d]{5}$/.test(zip)) {
            this.user.zip = zip;
        }
        if (/^\([\d]{3}\)[\d]{3}-[\d]{4}$/.test(phone)) {
            this.user.phone = phone;
        }
    };
    ProfileComponent.prototype.imgPicked = function (file) {
        this.picked = file;
        console.log(this.picked);
    };
    ProfileComponent.prototype.showChange = function (img, btn) {
        img.style.filter = "opacity(50%)";
        btn.hidden = false;
    };
    ProfileComponent.prototype.hideChange = function (img, btn) {
        img.style.filter = "";
        btn.hidden = true;
    };
    return ProfileComponent;
}());
ProfileComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-profile',
        template: __webpack_require__("../../../../../src/app/profile/profile.component.html"),
        styles: [__webpack_require__("../../../../../src/app/profile/profile.component.css")],
        providers: [__WEBPACK_IMPORTED_MODULE_2__home_home_service__["a" /* HomeService */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__home_home_service__["a" /* HomeService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__home_home_service__["a" /* HomeService */]) === "function" && _a || Object])
], ProfileComponent);

var _a;
//# sourceMappingURL=profile.component.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_23" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map