import { Injectable } from "@angular/core";
import { Follower } from "./follower";
import { FOLLOWERS } from "./faux-followers";

@Injectable()
export class FollowersService {

  getFollowers(): Promise<Follower[]> {
    return Promise.resolve(FOLLOWERS);
  }

  makeFollower(newFollower: string): Promise<Follower> {
    const follower: Follower = {
      name: newFollower,
      img: "http://lorempixel.com/384/202/",
      status: "Look who decided to show up"
    };

    return Promise.resolve(follower);
  }

}
