import { Injectable } from "@angular/core";
import { Post } from "./post";
import { POSTS } from "./faux-posts";

@Injectable()
export class PostService {

  getPosts(): Promise<Post[]> {
    return Promise.resolve(POSTS)
  }

}
