import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'RiceBook';
  loggedIn: boolean;
  userName: string;
  password: string;


  constructor(private router: Router, private activeRoute: ActivatedRoute) { };

  ngOnInit(): void {
    this.userName = fetch("username");
    this.password = fetch("password");

    this.loggedIn = Boolean(this.userName && this.password)

  }

  logIn(): void {
    if (this.userName && this.password) {
      this.loggedIn = true;

      window.localStorage.setItem("username", this.userName);
      window.localStorage.setItem("password", this.password);

      this.router.navigate(['home'], { relativeTo: this.activeRoute.root })
    }
  };

  logOut(): void {
    this.loggedIn = false;

    window.localStorage.removeItem("username");
    this.userName = "";
    window.localStorage.removeItem("password");
    this.password = "";

    this.router.navigate(['/'], { relativeTo: this.activeRoute.root })
  }

}


function fetch(key, def = "") {
  if (window.localStorage.getItem((key)) !== null) {
    return window.localStorage.getItem((key))
  } else {
    return def
  }
}
