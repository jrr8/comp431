import { Component, OnInit } from '@angular/core';
import { Post } from "../post/post";
import { PostService } from "../post/post.service";
import { Follower } from "../followers/follower";
import { FollowersService } from "../followers/followers.service";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [PostService, FollowersService]
})

export class HomeComponent implements OnInit {

	posts: Post[];
	followers: Follower[];
	status: string;
	isStatusBeingEdited: boolean;
	editOrSave: string;


  constructor( private postService: PostService, private followerService: FollowersService ) { }


  ngOnInit() {

    this.postService.getPosts().then(posts => this.posts = posts);
    this.followerService.getFollowers().then(followers => this.followers = followers);

    this.status = "I like Angular";
    this.isStatusBeingEdited = false;
    this.editOrSave = "Edit";

  }

  addPost(postArea: HTMLTextAreaElement) {
    const newPost: Post = {
      text: postArea.value,
      img: null,
      date: "2017-10-31",
      comments: [],
      author: "Dr. Funk"
    };

    this.posts.unshift(newPost);
    postArea.value = "";
  }

  updateStatus(newStatus: string): void {

    if (this.editOrSave === "Edit") {
      this.isStatusBeingEdited = true;
      this.editOrSave = "Save";

    } else {
      this.isStatusBeingEdited = false;
      this.editOrSave = "Edit";
      this.status = newStatus;
    }

  }

  clearPost(postArea: HTMLTextAreaElement): void {
    postArea.value = "";
  }

  addFollower(followerInput: HTMLInputElement) {
    this.followerService.makeFollower(followerInput.value).then(follower => {
      this.followers.unshift(follower);
      followerInput.value = "";
    });
  }

  delFollower(oldFollower: Follower) {
    const idx = this.followers.indexOf(oldFollower);

    if (idx > -1) {
      this.followers.splice(idx, 1);
    }
  }

}
