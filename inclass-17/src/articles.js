const getArticles = (req, res) => res.send(
    [
        {
            id: 1,
            author: "bob",
            text: "hey"
        },
        {
            id:2,
            author: "jane",
            text: "there"
        },
        {
            id: 3,
            author: "doe",
            text: "bro"
        }
    ]
);

const newArticle = (req, res) => {
    console.log('Payload received', req.body);
    res.send(req.body)
}

module.exports = (app) => {
    app.get("/articles", getArticles);
    app.get("articles/:id", getArticle)
    app.post("/article", newArticle)
}