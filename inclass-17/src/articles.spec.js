/*
 * Test suite for articles.js
 */
var expect = require('chai').expect;
var fetch = require('isomorphic-fetch');

var url = function (path) { return "http://localhost:3000" + path; };

describe('Validate Article functionality', function () {
    it('should give me three or more articles', function (done) {
        // IMPLEMENT ME
        fetch(url('/articles'))
            .then(function (res) {
                expect(res.status).to.eql(200);
                return res.text();
            })
            .then(function (body) {
                expect(JSON.parse(body).length).to.be.above(2);
            })
            .then(done)
            .catch(done);
        }, 200);
    it('should add two articles with successive article ids, and return the article each time', function (done) {
        // add a new article
        const firstNewArticle = {
            id: 4,
            author: "Dr. P",
            text: "hey man"
        };
        // verify you get the article back with an id
        // verify the content of the article
        // add a second article
        // verify the article id increases by one
        // verify the second artice has the correct content
        done(new Error('Not implemented'));
    }, 200);

    it('should return an article with a specified id', function (done) {
        fetch(url('/articles'))
            .then(function (res) {
                expect(res.status).to.eql(200);
                return(res.text());
            })
            .then(function (body) {

            })


        // then call GET /articles/id with the chosen id
        // validate that only one article is returned
        done(new Error('Not implemented'));
    }, 200);
    it('should return nothing for an invalid id', function (done) {
        // call GET /articles/id where id is not a valid article id, perhaps 0
        // confirm that you get no results
        done(new Error('Not implemented'));
    }, 200);
});
