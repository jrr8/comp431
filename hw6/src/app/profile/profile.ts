export class Profile {

    firstName: string;
    lastName: string;
    email: string;
    birthday: string;
    username: string;
    zip: string;
    phone: string;

}
