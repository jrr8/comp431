import { Component, OnInit } from '@angular/core';
import { Post } from '../post/post';
import { PostService } from '../post/post.service';
import { Follower } from '../followers/follower';
import { FollowersService } from '../followers/followers.service';
import { Profile } from '../profile/profile';
import { fetch } from '../app.component'


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [PostService, FollowersService]
})

export class HomeComponent implements OnInit {

  posts: Post[];
  followers: Follower[];
  user: Profile;
  status: string;
  isStatusBeingEdited: boolean;
  editOrSave: string;


  constructor( private postService: PostService, private followerService: FollowersService ) { }


  ngOnInit() {

    this.postService.getPosts().then(posts => this.posts = posts);
    this.followerService.getFollowers().then(followers => this.followers = followers);

    this.status = 'I like Angular';
    this.isStatusBeingEdited = false;
    this.editOrSave = 'Edit';

  }

  addPost(postArea: HTMLTextAreaElement) {
    this.postService.addPost(postArea, 'Dr. Funk', this.posts).then(posts => {
      this.posts = posts;
    });
  }

  updateStatus(newStatus: string): void {

    if (this.editOrSave === 'Edit') {
      this.isStatusBeingEdited = true;
      this.editOrSave = 'Save';

    } else {
      this.isStatusBeingEdited = false;
      this.editOrSave = 'Edit';
      this.status = newStatus;
    }

  }

  clearPost(postArea: HTMLTextAreaElement): void {
    postArea.value = '';
  }

  addFollower(followerInput: HTMLInputElement) {
    this.followerService.makeFollower(followerInput).then(follower => {
      this.followers.unshift(follower);
    });
  }

  delFollower(oldFollower: Follower) {
    this.followerService.removeFollower(oldFollower, this.followers).then(followers => {
      this.followers = followers
    })

  }

}
