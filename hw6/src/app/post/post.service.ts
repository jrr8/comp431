import { Injectable } from "@angular/core";
import { Post } from "./post";
import { POSTS } from "./faux-posts";

@Injectable()
export class PostService {

  getPosts(): Promise<Post[]> {
    return Promise.resolve(POSTS)
  }

  addPost(postArea: HTMLTextAreaElement, author: string, posts: Post[]): Promise<Post[]> {
    const newPost: Post = {
      text: postArea.value,
      img: null,
      date: function() {
        const date = new Date();
        return date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
      }(),
      comments: [],
      author: author
    };

    postArea.value = "";

    posts.unshift(newPost);

    return Promise.resolve(posts)
  }

}
