import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notfound',
  templateUrl: './404.component.html',
  styleUrls: ['./404.component.css']
})
export class NotfoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
